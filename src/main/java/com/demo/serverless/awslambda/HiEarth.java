package com.demo.serverless.awslambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

public class HiEarth {
    public String handle(User input, Context context) {
        LambdaLogger logger = context.getLogger();
        logger.log("Input : " + input);
        return "Hi earth " + input;
    }
}
