package com.demo.serverless.awslambda;

public class User {
   private String input;
    public String getInput() { return input; }
    public void setInput(String input) { this.input = input; }

    @Override
    public String toString() {
        return input;
    }
}